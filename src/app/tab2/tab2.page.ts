import { Component, ViewChild, ElementRef } from '@angular/core';
import { Storage } from '@ionic/storage';
import { AlertController, IonProgressBar } from '@ionic/angular';
import * as moment from 'moment';
import leaflet from 'leaflet';
import { PostsService } from 'src/app/services/posts.service';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})

export class Tab2Page {

  @ViewChild('map2') mapContainer: ElementRef;
  @ViewChild('bar') bar: IonProgressBar;

  constructor(
    public toastController: ToastController,
    public api: PostsService,
    public storage: Storage,
    public alertController: AlertController
  ) { }

  posts: any = [];
  map2: any;
  myLayer2: any;

  ngOnInit() {

  }

  async getData(o: any, cb: Function) {
    this.api.getData(o, function (e, r) {
      cb(e, r);
    });
  }

  async presentAlertConfirm() {
    var me = this;
    const alert = await this.alertController.create({
      header: 'Rivages 2.0',
      message: 'Voulez-vous vraiment supprimer tous les enregistrements ?',
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {

          }
        }, {
          text: 'OK',
          handler: () => {
            this.storage.set('posts', []).then(function () {
              me.storage.get('posts').then((posts) => {
                me.posts = posts;
              });
            });
          }
        }
      ]
    });

    await alert.present();
  }

  test_post() {
    var me = this;
    var posts = this.posts;
    function loader(posts, ndx, cb) {
      me.bar.value = (ndx / posts.length);
      if (!posts[0]) return cb();
      me.getData(posts[0], function () {
        posts.shift();
        me.posts = posts;
        me.storage.set('posts', posts).then(function () {
          loader(posts, ndx + 1, cb);
        });
      });
    };
    loader(posts, 0, async function () {
      me.bar.value = 0;
      const toast = await me.toastController.create({
        showCloseButton: true,
        message: 'Merci. Vos contributions ont été bien reçues',
        position: 'top',
        closeButtonText: 'OK'
      });
      toast.present();
    });

  }

  rmTo($event) {
    this.posts.splice($event, 1);
    this.storage.set('posts', this.posts).then(function () {
    });
  }

  upTo($event) {
    var me = this;
    me.getData(this.posts[$event], function (e, r) {
      console.log(e);
      console.log(r);
      me.posts.splice($event, 1);
      me.storage.set('posts', me.posts).then(async function () {
        const toast = await me.toastController.create({
          showCloseButton: true,
          message: 'Merci. Votre contribution a été bien reçue',
          position: 'top',
          closeButtonText: 'OK'
        });
        toast.present();
      });
    });
  }

  deleteAll() {
    this.presentAlertConfirm();
  }

  ionViewDidEnter() {
    if (!this.map2) {
      this.map2 = leaflet.map("map2", {
        attributionControl: false
      }).fitWorld();

      leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attributions: 'www.tphangout.com',
        maxZoom: 18
      }).addTo(this.map2);

      this.myLayer2 = leaflet.geoJSON().addTo(this.map2);
    };
    this.storage.get('posts').then((posts) => {
      if (!posts) posts = [];
      for (var i = 0; i < posts.length; i++) {
        posts[i].stamp = moment(posts[i].stamp).locale('fr').fromNow();
      };

      this.posts = posts.reverse();

    });

  }
}
