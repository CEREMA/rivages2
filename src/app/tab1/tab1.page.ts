import { Component, ViewChild, ElementRef } from '@angular/core';
import leaflet from 'leaflet';
import { ActionSheetController, Platform, ToastController, IonFabButton, IonFabList, ModalController } from '@ionic/angular';
import { GeoService } from '../services/geo.service';
import { Storage } from '@ionic/storage';
import { UUID } from 'angular2-uuid';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { SwUpdate } from '@angular/service-worker';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})

export class Tab1Page {

  constructor(private updates: SwUpdate, public camera: Camera, public geolocation: Geolocation, public actionSheetController: ActionSheetController, public platform: Platform, public toastController: ToastController, public storage: Storage, public modalController: ModalController) {

  }

  @ViewChild('map') mapContainer: ElementRef;
  @ViewChild('add') addBtn: IonFabButton;
  @ViewChild('pause') pauseBtn: IonFabButton;
  @ViewChild('fab') fabBtn: IonFabList;
  @ViewChild('info') infopanel: ElementRef;
  @ViewChild('infogps') infogps: ElementRef;
  @ViewChild('KPlay') KPlay: ElementRef;
  @ViewChild('KPause') KPause: ElementRef;
  @ViewChild('file') Photo: any;

  video: any;
  map: any;
  videoDevice: any;
  myLayer: any;
  hideMe: any = false;
  hideIt: any = true;
  timer: any = true;
  Record: any = {};
  Pause: any = false;
  lastCoords: any = [];
  markerGroup: any;
  isBegin: any = true;
  onRecord: any = false;
  POS: any = [];

  initCamera(config: any) {
    var browser = <any>navigator;

    browser.getUserMedia = (browser.getUserMedia ||
      browser.webkitGetUserMedia ||
      browser.mozGetUserMedia ||
      browser.msGetUserMedia);

    browser.mediaDevices.getUserMedia(config).then(stream => {
      console.log(stream);
      //this.video.src = window.URL.createObjectURL(stream);
      console.log(this.video.src);
      this.video.play();
    });
  }

  ngOnInit() {
    this.updates.available.subscribe(event => {
      this.updates.activateUpdate().then(() => document.location.reload());
    })
    this.updates.checkForUpdate();

  }

  addMarker() {
    var me = this;
    navigator.geolocation.getCurrentPosition(function (position) {
      // position.coords.latitude, position.coords.longitude
      let marker: any = leaflet.marker([position.coords.latitude, position.coords.longitude]).on('click', () => {
        alert('Marker clicked');
      })
      me.markerGroup.addLayer(marker);
    }, function () { }, { maximumAge: 0, timeout: 5000, enableHighAccuracy: true });
  }



  onStartRecord(type: any) {
    this.onRecord = true;
    let me: any = this;
    this.hideMe = true;
    this.infopanel.nativeElement.style.display = "";
    this.presentToast('Enregistrement en cours...');
    this.Pause = false;
    this.Record = {
      uuid: UUID.UUID(),
      stamp: new Date(),
      type: type,
      geojson: new GeoService(),
      limits: [],
      pictures: [],
      sequences: []
    };

    this.POS = [];

    this.addMarker();

    this.timer = setInterval(function () {

      me.geolocation.getCurrentPosition({
        enableHighAccuracy: true
      }).then((position) => {
        if (!me.Pause) {
          me.KPlay.nativeElement.style.display = "";
          me.KPause.nativeElement.style.display = "none";
          console.log(position.coords);
          // latitude longitude
          me.lastCoords = [position.coords.longitude, position.coords.latitude];
          me.POS.push(me.lastCoords);

          if (me.POS.length > 10) {
            console.log(JSON.stringify({
              "type": "LineString",
              "coordinates": me.POS
            }));

            me.myLayer.addData({
              "type": "LineString",
              "coordinates": me.POS
            });
            console.log(JSON.stringify({
              "type": "LineString",
              "coordinates": me.POS
            }));
            me.POS = [];
          };

          if (position.coords.accuracy < 10) {
            me.Record.geojson.add(position);
            me.infogps.nativeElement.innerHTML = "SEQ: " + (me.Record.sequences.length + 1) + "<BR>LON: " + position.coords.longitude + "<BR>LAT: " + position.coords.latitude + "<BR>Précision: " + Math.trunc(position.coords.accuracy) + " - " + me.Record.geojson.json.features.length + ' point(s)';
          } else {
            me.infogps.nativeElement.innerHTML = "précision insuffisante.";
          }
        }
      }).catch((error) => {
        alert('Error getting location');
      });


    }, 1000);
  }

  async presentActionSheet(zz) {

    function update(me, str) {
      me.Record.type = str;
      me.Record.geojson = new GeoService();
      me.KPlay.nativeElement.style.display = "";
      me.KPause.nativeElement.style.display = "none";
      me.presentToast('Enregistrement en cours...');
      me.hideIt = true;
      me.Pause = false;
      me.addMarker();
    };
    const actionSheet = await this.actionSheetController.create({
      header: 'Type de trait de côte',
      buttons: [{
        text: 'Limite du jet de rive',
        handler: () => {
          if (zz == 1) update(this, 'Limite du jet de rive'); else this.onStartRecord('Limite du jet de rive');
        }
      }, {
        text: 'Limite de végétation (hors dune)',
        handler: () => {
          if (zz == 1) update(this, 'Limite de végétation (hors dune)'); else this.onStartRecord('Limite de végétation (hors dune)');
        }
      }, {
        text: 'Limite côté mer de la végétation dunaire',
        handler: () => {
          if (zz == 1) update(this, 'Limite côté mer de la végétation dunaire'); else this.onStartRecord('Limite côté mer de la végétation dunaire');
        }
      }, {
        text: 'Cordon de galets',
        handler: () => {
          if (zz == 1) update(this, 'Cordon de galets'); else this.onStartRecord('Cordon de galets');
        }
      },
      {
        text: 'Fond de plage',
        handler: () => {
          if (zz == 1) update(this, 'Fond de plage'); else this.onStartRecord('Fond de plage');
        }
      }, {
        text: 'Pied de dune - Pied de falaise dunaire',
        handler: () => {
          if (zz == 1) update(this, 'Pied de dune - Pied de falaise dunaire'); else this.onStartRecord('Pied de dune - Pied de falaise dunaire');
        }
      }, {
        text: 'Dernière lasse de haute mer (après tempête)',
        handler: () => {
          if (zz == 1) update(this, 'Dernière lasse de haute mer (après tempête)'); else this.onStartRecord('Dernière lasse de haute mer (après tempête)');
        }
      }, {
        text: 'Autre',
        handler: () => {
          if (zz == 1) update(this, 'Autre'); else this.onStartRecord('Autre');
        }
      },
      {
        text: 'Annuler',
        icon: 'close',
        role: 'cancel',
        handler: () => {

        }
      }]
    });
    await actionSheet.present();
  }

  async presentToast(msg: string) {
    const toast = await this.toastController.create({
      message: msg,
      position: "top",
      duration: 2000
    });
    toast.present();
  }

  ionViewDidEnter() {

    this.addBtn.disabled = true;
    this.loadmap();

  }

  setFileName($event) {
    var file = this.Photo.nativeElement.files[0];
    var reader = new FileReader();
    var me = this;

    reader.addEventListener("load", function () {
      console.log(reader.result);
      me.Record.pictures.push(reader.result);
    }, false);

    if (file) {
      reader.readAsDataURL(file);
    }
  }

  onClickPhoto($event) {
    this.Photo.nativeElement.click();
  }

  onClickPause($event) {
    var me = this;
    me.myLayer.addData({
      "type": "LineString",
      "coordinates": me.POS
    });
    me.POS = [];
    this.presentToast('Enregistrement en pause...');
    this.KPlay.nativeElement.style.display = "none";
    this.KPause.nativeElement.style.display = "";
    this.hideIt = false;
    this.Record.limits.push(this.Record.type);
    this.Record.sequences.push(this.Record.geojson.json);
    this.Pause = true;
    let marker: any = leaflet.marker(me.lastCoords.reverse()).on('click', () => {
      alert('Marker clicked');
    })
    this.markerGroup.addLayer(marker);
  }

  onClickPlay($event) {
    let me = this;
    return this.presentActionSheet(1);
    this.Record.geojson = new GeoService();
    this.KPlay.nativeElement.style.display = "";
    this.KPause.nativeElement.style.display = "none";
    this.presentToast('Enregistrement en cours...');
    this.hideIt = true;
    this.Pause = false;
    this.addMarker();

  }

  onClickStop($event) {
    this.onRecord = false;
    var me = this;
    this.KPlay.nativeElement.style.display = "";
    this.KPause.nativeElement.style.display = "none";
    this.presentToast('Fin de l\'enregistrement.');
    this.hideMe = false;
    clearInterval(this.timer);
    if (!this.Pause) {
      this.Record.sequences.push(this.Record.geojson.json);
      this.Record.limits.push(this.Record.type);
    };
    this.Pause = false;
    delete this.Record.geojson;
    this.infopanel.nativeElement.style.display = "none";
    this.markerGroup.clearLayers();
    this.isBegin = false;
    this.storage.get('posts').then((val) => {
      if (!val) val = [];
      me.myLayer.remove();
      val.push(me.Record);
      me.storage.set('posts', val);
      me.Record = {};
    });
  }

  onClickStart($event) {
    this.Pause = false;
    return this.presentActionSheet(0);
  }

  loadmap() {
    let me: any = this;
    if (!this.map) {
      this.map = leaflet.map("map", {
        attributionControl: false
      }).fitWorld();

      leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attributions: 'www.tphangout.com',
        maxZoom: 18
      }).addTo(this.map);

      this.myLayer = leaflet.geoJSON().addTo(this.map);
    };

    if (this.onRecord) return;
    navigator.geolocation.getCurrentPosition(function (position) {
      me.addBtn.disabled = false;
      me.map.locate({
        setView: true,
        maxZoom: 18,
        enableHighAccuracy: true,
        maximumAge: 60000
      }).on('locationfound', (e) => {
        me.markerGroup = leaflet.featureGroup();
        me.map.addLayer(me.markerGroup);

      }).on('locationerror', (err) => {
        alert("Nous n'arrivons pas à vous positionner.");
      })
    }, (err) => { alert("Nous n'arrivons pas à vous positionner."); }, { maximumAge: 0, timeout: 5000, enableHighAccuracy: true });


  }
}
