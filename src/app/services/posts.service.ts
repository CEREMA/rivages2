import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import ShortUniqueId from 'short-unique-id';

const apiUrl = "https://rivages.siipro.fr";
//const apiUrl = "http://127.0.0.1:3000";

@Injectable({
  providedIn: 'root'
})

export class PostsService {

  constructor(private httpClient: HttpClient) { }

  b64toBlob(b64Data: string, contentType: string = "", sliceSize: any = 512) {


    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);

      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      var byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  drawImageScaled(img: any, ctx: any, canvas: any) {
    var hRatio = canvas.width / img.width;
    var vRatio = canvas.height / img.height;
    var ratio = Math.min(hRatio, vRatio);
    var centerShift_x = (canvas.width - img.width * ratio) / 2;
    var centerShift_y = (canvas.height - img.height * ratio) / 2;
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.drawImage(img, 0, 0, img.width, img.height,
      centerShift_x, centerShift_y, img.width * ratio, img.height * ratio);
  }

  encode(pictures: any, ndx: any, cb: Function) {
    const width = 500;
    const height = 500;
    if (!pictures[ndx]) return cb(pictures);
    var pic = pictures[ndx];
    var ext = pic.split(';')[0].split(':')[1];
    if (ext.indexOf('jpeg') > -1) ext = "image/jpg";
    pic = pic.split(',')[1];
    var blob = this.b64toBlob(pic, ext);
    const reader = new FileReader();
    reader.readAsDataURL(blob);
    reader.onload = (event: Event) => {
      var img: any = new Image();
      img.src = reader.result;
      img.onload = () => {
        var elem = document.createElement('canvas');
        elem.width = width;
        elem.height = height;
        const ctx = elem.getContext('2d');
        this.drawImageScaled(img, ctx, elem);
        var data = ctx.canvas.toDataURL('image/jpeg');
        pictures[ndx] = data;
        this.encode(pictures, ndx + 1, cb);
      }
    }
  }

  getData(o: any, cb: Function) {
    var me = this;
    this.encode(o.pictures, 0, function (pix) {
      var imei = window.localStorage.getItem('imei');
      if (!imei) {
        var uid = new ShortUniqueId();
        var imei = uid.randomUUID(13);
        window.localStorage.setItem('imei', imei);
      };

      o.imei = imei;
      o.pictures = pix;
      me.httpClient.post(apiUrl + "/up", o)
        .subscribe(
          data => {
            cb(null, data);
          },
          error => {
            console.log(error);
            cb(error);
          }

        );
    });
    return true;
  }

}

